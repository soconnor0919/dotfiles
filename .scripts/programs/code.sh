#!/bin/bash

sudo snap install code --classic
code --install-extension visualstudioexptteam.vscodeintellicode 
code --install-extension eamodio.gitlens 
code --install-extension vscjava.vscode-java-pack 
code --install-extension siddharthkp.codesandbox-black
code --install-extension pkief.material-icon-theme
code --install-extension mads-hartmann.bash-ide-vscode
rm -rf /home/"$USER"/.config/VSCodium/User/settings.json
touch /home/"$USER"/.config/VSCodium/User/settings.json
cat /home/"$USER"/.programs/codium/settings.json > /home/"$USER"/.config/VSCodium/User/settings.json
sudo cat /home/"$USER"/.programs/codium/product.json > /usr/share/codium/resources/app/product.json
clear
